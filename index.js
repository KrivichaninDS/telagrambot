var TeleBot = require('telebot');
var nconf = require('./config');
var getRaspAp1Routes = require('./rasp.ap1');
var fs = require('fs');

var botID = nconf.get('BOT_ID');
var bot = new TeleBot(botID);
var chatId;

/*
 In the future some route update schedule
 */
// update();

function handleError(err) {
  sendMessage(`${err.name}: ${err.message}`);
}

function update() {
  getRaspAp1Routes((err, data) => {
    if (err) return handleError(err);
    data ? fs.writeFile('./outputFiles/routes.json', data, (err) => {
      if (err) return handleError(err);
      sendMessage(`Расписание обновлено`);
    }) : '';
  })
}

bot.on('/go', msg => {
  chatId ? '' : chatId = msg.chat.id;
  bot.sendMessage(msg.from.id, 'Ky-ky')
});

function sendMessage(msg) {
  chatId ? bot.sendMessage(chatId, msg) : console.log(msg);
}

bot.connect();
