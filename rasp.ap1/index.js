var nconf = require('../config');
var async = require('async');
var through = require('through2');

var getPage = require('./requests');
var ParseRoutes = require('./parseRoutes');
var ParseSingleRoute = require('./parseSingleRoute');

const parseRoutes = ParseRoutes();
const raspAp1Stream = getPage(nconf.get('rasp.ap1_url'), nconf.get('requestOptions'));

const getRaspAp1Routes = (result) => {
  raspAp1Stream
    .on('response', function (response) {
      console.log(response.statusCode);
    })
    .on('error', (err) => {
      result(err);
    })
    .pipe(parseRoutes)
    .on('error', (err) => {
      result(err);
    })
    .pipe(through(function (data, enc, cb) {
      var routes = [];
      var array = JSON.parse(data.toString());

      async.forEachOf(array, function (item, key, asyncCallBack) {
        getPage(nconf.get('rasp.ap1_url') + item.link, nconf.get('requestOptions'))
          .pipe(ParseSingleRoute())
          .pipe(through(function (chunk, enc, cb) {
            var obj = JSON.parse(chunk.toString());
            routes.push(obj);

            cb();
            asyncCallBack();
          }));

      }, function (err) {
        if (err) throw result(err);
        result(null, JSON.stringify(routes));
        cb();
      });
    }))
    .on('error', (err) => {
      result(err);
    });

};

module.exports = getRaspAp1Routes;
