var request = require('request');

var getPage = function (uri, options) {
    return request.get(uri, {
        timeout: options.time
    })
};

module.exports = getPage;